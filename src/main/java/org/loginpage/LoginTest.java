package org.loginpage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Duration;

public class LoginTest {
    WebDriver driver;

    @Test(priority = 0)
    public void browserTest() {
        try {
            driver = new ChromeDriver();
            driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");

        } catch (Exception e0) {
            System.out.println(e0);
            try {
                driver = new FirefoxDriver();
                driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");

            } catch (Exception e1) {
                System.out.println(e1);
                try {
                    driver = new EdgeDriver();
                    driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");

                } catch (Exception e2) {
                    System.out.println(e2);
                    Assert.fail("Supported Browser is not available in your machine");
                }
            }
        }
    }

    @Test(priority = 1)
    public void textBoxTest() throws InterruptedException {
        new WebDriverWait(driver, Duration.ofSeconds(10)).until(webDriver -> webDriver.getPageSource().contains("Login"));
        WebElement username = driver.findElement(By.name("username"));
        Assert.assertTrue(username.isDisplayed());
        WebElement password = driver.findElement(By.name("password"));
        Assert.assertTrue(password.isDisplayed());

        username.sendKeys("Admin");
        password.sendKeys("admin123");

        WebElement btn = driver.findElement(By.xpath("/html/body/div/div[1]/div/div[1]/div/div[2]/div[2]/form/div[3]/button"));
        btn.click();
    }
}
